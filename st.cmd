# -----------------------------------------------------------------------------
# EPICS IOC startup command to initialize Rohde & Schwarz NRPZ instruments
#   driver.
#
# Sensors Hub (controller)
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group, 2020
# -----------------------------------------------------------------------------
# Author  : Douglas Bezerra Beniz
# email   : douglas.bezerra.beniz@ess.eu
# -----------------------------------------------------------------------------

require(rs_nrpz, master)
require(common, 0.3.0)

# ---
epicsEnvSet("LOCATION",         "UTG")
epicsEnvSet("DEVICE_NAME",      "RS_NRPZ-01")
epicsEnvSet("PREFIX",           "$(LOCATION):$(DEVICE_NAME)")
epicsEnvSet("PORT",             "$(DEVICE_NAME)")

# Create a R&S NRP-Z driver
RS_NRPZConfig("$(PORT)", 0, 0)

# Loading the hub/controller database
iocshLoad("$(rs_nrpz_DIR)rs_nrpz_sensors_hub.iocsh", "P=$(PREFIX):,R=,PORT=$(PORT),ADDR=0")

# Loading all channels
iocshLoad("$(rs_nrpz_DIR)rs_nrpz_sensor_channel.iocsh", "P=$(PREFIX),R=-Ch0:,PORT=$(PORT),ADDR=0")
iocshLoad("$(rs_nrpz_DIR)rs_nrpz_sensor_channel.iocsh", "P=$(PREFIX),R=-Ch1:,PORT=$(PORT),ADDR=1")
iocshLoad("$(rs_nrpz_DIR)rs_nrpz_sensor_channel.iocsh", "P=$(PREFIX),R=-Ch2:,PORT=$(PORT),ADDR=2")
iocshLoad("$(rs_nrpz_DIR)rs_nrpz_sensor_channel.iocsh", "P=$(PREFIX),R=-Ch3:,PORT=$(PORT),ADDR=3")
iocshLoad("$(rs_nrpz_DIR)rs_nrpz_sensor_channel.iocsh", "P=$(PREFIX),R=-Ch4:,PORT=$(PORT),ADDR=4")
iocshLoad("$(rs_nrpz_DIR)rs_nrpz_sensor_channel.iocsh", "P=$(PREFIX),R=-Ch5:,PORT=$(PORT),ADDR=5")
iocshLoad("$(rs_nrpz_DIR)rs_nrpz_sensor_channel.iocsh", "P=$(PREFIX),R=-Ch6:,PORT=$(PORT),ADDR=6")
iocshLoad("$(rs_nrpz_DIR)rs_nrpz_sensor_channel.iocsh", "P=$(PREFIX),R=-Ch7:,PORT=$(PORT),ADDR=7")

# Loading COMMON modules
iocshLoad("$(common_DIR)e3-common.iocsh")

# Starting the IOC...
iocInit()

